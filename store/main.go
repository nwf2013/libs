package store

import (
	"github.com/VictoriaMetrics/fastcache"
)

var provider *fastcache.Cache

func New() {
	provider = fastcache.New(100000)
}

func Get(dst, k []byte) []byte {
	return provider.Get(dst, k)
}

func GetBig(dst, k []byte) []byte {
	return provider.GetBig(dst, k)
}

func Has(k []byte) bool {
	return provider.Has(k)
}

func Set(k, v []byte) {
	provider.Set(k, v)
}

func SetBig(k, v []byte) {
	provider.SetBig(k, v)
}

func Reset() {
	provider.Reset()
}