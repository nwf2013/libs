package form

import (
	//"fmt"
	"sync"
	"math"
	"errors"
	"strings"
	"strconv"
	"github.com/valyala/fasthttp"
	"github.com/modern-go/reflect2"
)

type Tags struct {
	Rule string `json:"rule"`
	Min  int    `json:"min"`
	Max  int    `json:"max"`
	Msg  string `json:"msg"`
}

type field struct {
	Offset    uintptr
	FieldType string
	Name      string
	Tag       Tags
}

type validate struct {
	StructName string
	Fields     map[string]field
}

var pord sync.Map

func Bind(ctx *fasthttp.RequestCtx, typ interface{}) (interface{}, error) {

	var opt validate
	
	// StructType
	valType    := reflect2.TypeOf(typ).(reflect2.StructType)
	record, ok := pord.Load(valType.String());
	
	if !ok {
		
		c       := valType.NumField()
	
		opt.Fields = make(map[string]field)
	
		for i := 0; i < c; i++ {
	
			f    := valType.Field(i)
			data := field{}
	
			data.Name      = f.Name()
			data.Offset    = f.Offset()
			data.FieldType = f.Type().Kind().String()
			
			
			attr := f.Tag()
			rule := attr.Get("rule")
			min  := attr.Get("min")
			max  := attr.Get("max")
			msg  := attr.Get("msg")

			if len(rule) == 0 {
				return nil, errors.New("Rule Not defined")
			}
			if len(min) == 0 {
				data.Tag.Min = 0
			} else {
				val, err := strconv.Atoi(min)
				if err != nil {
					return nil, errors.New("Min Not integer")
				}
				data.Tag.Min = val
			}

			if len(max) == 0 {
				data.Tag.Max = 0
			} else {
				val, err := strconv.Atoi(max)
				if err != nil {
					return nil, errors.New("Max Not integer")
				}
				data.Tag.Max = val
			}
			if len(msg) == 0 {
				data.Tag.Msg  = msg
			} else {
				data.Tag.Msg  = msg
			}
			data.Tag.Rule = strings.ToLower(rule)

			opt.Fields[strings.ToLower(f.Name())] = data
		}
	
		pord.Store(valType.String(), opt)
	} else {
		opt = record.(validate)
	}

	
	for key, value := range opt.Fields {

		val := ""
		if string(ctx.Method()) == "GET" {
			val = string(ctx.QueryArgs().Peek(key))
		} else if string(ctx.Method()) == "POST" {
			val = string(ctx.PostArgs().Peek(key))
		}

		if val == "" {
			return nil, errors.New(key+" not found")
		}

		if value.Tag.Rule == "digit" {
			if !checkStringDigit(val) || !checkIntScope(val, value.Tag.Min, value.Tag.Max) {
				return nil, errors.New(value.Tag.Msg)
			}
		} else if value.Tag.Rule == "alnum" {
			if !checkStringAlnum(val) || !checkStringLength(val, value.Tag.Min, value.Tag.Max) {
				return nil, errors.New(value.Tag.Msg)
			}
		} else if value.Tag.Rule == "priv" {
			if !isPriv(val) {
				return nil, errors.New(value.Tag.Msg)
			}
		} else if value.Tag.Rule == "chn" {
			if !checkStringCHN(val) {
				return nil, errors.New(value.Tag.Msg)
			}
		} else if value.Tag.Rule == "isEmpty" {
			if !checkStringLength(val, value.Tag.Min, math.MaxInt32) {
				return nil, errors.New(value.Tag.Msg)
			}
		}

		set(&value, typ, value.Name, val)

	}

	return typ, nil
}

// Verify 表单数据检测
func Verify(rules map[string]Tags) (string, bool) {

	for k, val := range rules {
		if val.Rule == "alpha" && (!checkStringAlpha(k) || !checkStringLength(k, val.Min, val.Max)) {
			return val.Msg, false
		} else if val.Rule == "digit" && (!checkStringDigit(k) || !checkStringLength(k, val.Min, val.Max)) {
			return val.Msg, false
		} else if val.Rule == "alnum" && (!checkStringAlnum(k) || !checkStringLength(k, val.Min, val.Max)) {
			return val.Msg, false
		} else if val.Rule == "string" && !checkStringLength(k, val.Min, val.Max) {
			return val.Msg, false
		} else if val.Rule == "empty" && !checkStr(k) {
			return val.Msg, false
		} else if val.Rule == "utf8" && (!checkStringCHN(k) || !checkStringLength(k, val.Min, val.Max)) {
			return val.Msg, false
		} else if val.Rule == "bool" && !checkBool(k) {
			return val.Msg, false
		} else if val.Rule == "float" && !checkFloat(k) {
			return val.Msg, false
		} else if val.Rule == "name" && !checkName(k) {
			return val.Msg, false
		} else if val.Rule == "password" && !checkPassword(k) {
			return val.Msg, false
		} else if val.Rule == "mail" && !checkEmail(k) {
			return val.Msg, false
		} else if val.Rule == "qq" && !checkQQ(k) {
			return val.Msg, false
		} else if val.Rule == "phone" && !checkPhone(k) {
			return val.Msg, false
		} else if val.Rule == "date" &&  !checkDate(k) {
			return val.Msg, false 
		} else if val.Rule == "time" &&  !checkTime(k) {
			return val.Msg, false 
		} else if val.Rule == "datetime" &&  !checkDateTime(k) {
			return val.Msg, false 
		}
	}

	return "", true
}
