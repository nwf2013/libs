package form

import (
	//"errors"
	"fmt"
	//"math"
	"strconv"
	"unsafe"

	//"github.com/valyala/fasthttp"
)

type empty struct {
	etype *struct{}
	ptr   unsafe.Pointer
}

func set(opt *field, obj interface{}, key string, value interface{}) error {

	ptr := (*empty)(unsafe.Pointer(&obj)).ptr
	ptr = unsafe.Pointer(uintptr(ptr) + opt.Offset)
	switch opt.FieldType {
	case "int", "int8", "int16", "int32", "int64":
		val, _ := strconv.ParseInt(value.(string), 10, 64)

		*(*int64)(unsafe.Pointer(ptr)) = val
	case "uint", "uint8", "uint16", "uint32", "uint64", "uintptr":
		val, _ := strconv.ParseUint(value.(string), 10, 64)
		*(*uint64)(unsafe.Pointer(ptr)) = val
	case "float32":
		val, _ := strconv.ParseFloat(value.(string), 32)
		*(*float32)(unsafe.Pointer(ptr)) = float32(val)
	case "float64":
		val, _ := strconv.ParseFloat(value.(string), 64)
		*(*float64)(unsafe.Pointer(ptr)) = val
	case "string":

		*(*string)(unsafe.Pointer(ptr)) = value.(string)
	case "bool":

		val := false
		if value.(string) == "true" || value.(string) == "1" {
			val = true
		}
		*(*bool)(unsafe.Pointer(ptr)) = val
	case "slice-string", "slice-int", "slice-uint8":
		*(*[]interface{})(unsafe.Pointer(ptr)) = value.([]interface{})
	case "slice-float32", "slice-float64":
		*(*[]float64)(unsafe.Pointer(ptr)) = value.([]float64)
	default:
		return error(fmt.Errorf("暂不支持对%s进行赋值", opt.FieldType))
	}
	return nil
}

/*
// Check 数据表数据入库检测
func Check(ctx *fasthttp.RequestCtx, typ interface{}, rules map[string]Tags, name string) error {

	opt := vec[name]

	for key, value := range opt.Fields {

		if r, ok := rules[key]; ok {

			val := ""
			if string(ctx.Method()) == "GET" {
				val = string(ctx.QueryArgs().Peek(key))
			} else {
				val = string(ctx.PostArgs().Peek(key))
			}

			if val == "" {
				return errors.New(r.Msg)
			}

			//验证
			if r.Rule == "digit" {
				if !checkStringDigit(val) || !checkIntScope(val, r.Min, r.Max) {
					return errors.New(r.Msg)
				}
			} else if r.Rule == "alnum" {
				if !checkStringAlnum(val) || !checkStringLength(val, r.Min, r.Max) {
					return errors.New(r.Msg)
				}
			} else if r.Rule == "priv" {
				if !isPriv(val) {
					return errors.New(r.Msg)
				}
			} else if r.Rule == "chn" {
				if !checkStringCHN(val) {
					return errors.New(r.Msg)
				}
			} else if r.Rule == "isEmpty" {
				if !checkStringLength(val, r.Min, math.MaxInt32) {
					return errors.New(r.Msg)
				}
			}

			set(&value, typ, value.Name, val)
		}

	}

	return nil
}
*/