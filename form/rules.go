package form

import (
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"
)

//判断字符是否为数字
func isDigit(r rune) bool {
	return '0' <= r && r <= '9'
}

//判断字符是否为英文字符
func isAlpha(r rune) bool {

	if r >= 'A' && r <= 'Z' {
		return true
	} else if r >= 'a' && r <= 'z' {
		return true
	}
	return false
}

func isPriv(s string) bool {

	if s == "" {
		return false
	}

	for _, r := range s {
		if (r < 'A' || r > 'Z') && (r < 'a' || r > 'z') && r != '_' {
			return false
		}
	}

	return true
}

//匹配电子邮箱
func checkEmail(str string) bool {
	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*`
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(str)
}

//匹配qq
func checkQQ(str string) bool {
	pattern := `^[1-9]\d{4,10}`
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(str)
}

//匹配手机号
func checkPhone(str string) bool {
	pattern := `^1\d{10}`
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(str)
}

//匹配用户名格式
func checkName(str string) bool {
	pattern := `^[0-9A-Za-z]{4,12}$`
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(str)
}

//匹配用户名格式
func checkPassword(str string) bool {
	pattern := `^[0-9A-Za-z]{4,12}$`
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(str)
}

//匹配值是否为空
func checkStr(str string) bool {
	n := len(str)
	if n <= 0 {
		return false
	}
	return true
}

//判断是否为bool
func checkBool(str string) bool {

	_, err := strconv.ParseBool(str)
	if err != nil {
		return false
	}
	return true
}

//判断是否为float
func checkFloat(str string) bool {

	_, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return false
	}
	return true
}

//判断长度
func checkLength(str string, min, max int) bool {

	if min == 0 && max == 0 {
		return true
	}

	n := len(str)
	if n < min || n > max {
		return false
	}

	return true
}

//判断字符串长度
func checkStringLength(val string, _min, _max int) bool {

	if _min == 0 && _max == 0 {
		return true
	}

	count := utf8.RuneCountInString(val)
	if count < _min || count > _max {

		return false
	}
	return true
}

//判断数字范围
func checkIntScope(val string, _min, _max int) bool {

	_val, err := strconv.Atoi(val)
	if err != nil {
		return false
	}

	if _val < _min || _val > _max {
		return false
	}

	return true
}

//判断是否全为数字
func checkStringDigit(s string) bool {

	if s == "" {
		return false
	}
	for _, r := range s {
		if r < '0' || r > '9' {
			return false
		}
	}
	return true
}

//判断是否全为数字
func CheckStringDigit(s string) bool {

	if s == "" {
		return false
	}
	for _, r := range s {
		if r < '0' || r > '9' {
			return false
		}
	}
	return true
}

//判断是不是中文
func checkStringCHN(str string) bool {

	for _, r := range str {
		if unicode.Is(unicode.Scripts["Han"], r) || (regexp.MustCompile("[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b]").MatchString(string(r))) {
			return true
		}
	}
	return false
}

//判断是否全英文字母
func checkStringAlpha(s string) bool {

	if s == "" {
		return false
	}

	for _, r := range s {
		if (r < 'A' || r > 'Z') && (r < 'a' || r > 'z') {
			return false
		}
	}

	return true
}

//判断是否全为英文字母和数字组合
func CheckStringAlnum(s string) bool {

	if s == "" {
		return false
	}
	for _, r := range s {
		if !isDigit(r) && !isAlpha(r) {
			return false
		}
	}
	return true
}

//判断是否全为英文字母和数字组合
func checkStringAlnum(s string) bool {

	if s == "" {
		return false
	}
	for _, r := range s {
		if !isDigit(r) && !isAlpha(r) {
			return false
		}
	}
	return true

}


//匹配日期格式"YYYY-MM-DD"
func checkDate(str string) bool {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	_, err := time.ParseInLocation("2006-01-02", str, loc)
	if  err != nil {
		return false
	}
	return true
}

//检查日期格式"YYYY-MM-DD"
func CheckDate(str string) bool {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	_, err := time.ParseInLocation("2006-01-02", str, loc)
	if  err != nil {
		return false
	}
	return true
}

//匹配时间 "HH:ii" or "HH:ii:ss"
func checkTime(str string) bool {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	_, err := time.ParseInLocation("15:04:05", str, loc)
	if  err != nil {
		return false
	}
	return true
}

//匹配日期时间"YYYY-MM-DD HH:ii:ss"
func checkDateTime(str string) bool {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	_, err := time.ParseInLocation("2006-01-02 15:04:05", str, loc)
	if  err != nil {
		return false
	}
	return true
}

//检查日期时间格式"YYYY-MM-DD HH:ii:ss"
func CheckDateTime(str string) bool {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	_, err := time.ParseInLocation("2006-01-02 15:04:05", str, loc)
	if  err != nil {
		return false
	}
	return true
}

//检查会员真实姓名格式
func CheckRealName(realName string, min int, max int, minE int, maxE int) bool {

	realName = strings.Trim(realName, "")
	count := utf8.RuneCountInString(realName)
	matchCHN, chnErr := regexp.MatchString("^[\u4e00-\u9fa5]+([·•][\u4e00-\u9fa5]+)*$", realName)
	if matchCHN && chnErr == nil {
		if count < min || count > max {
			return false
		}
	}
	matchEN, enErr := regexp.MatchString("^[a-zA-Z]+([\\s·•]?[a-zA-Z]+)*$", realName)
	if matchEN && enErr == nil {
		if count < minE || count > maxE {
			return false
		}
	}
	if (!matchCHN || chnErr != nil) && (!matchEN || enErr != nil) {
		return false
	}

	return true
}


func CheckMoney(money string) bool {

	//金额小数验证
	_, errm := strconv.Atoi(money)
	if errm != nil {
		return false
	}
	_, errm = strconv.ParseFloat(money, 64)
	if errm != nil {
		return false
	}
	return true
}

//判断字符串是不是数字
func CtypeDigit(s string) bool {

	if s == "" {
		return false
	}
	for _, r := range s {
		if !isDigit(r) {
			return false
		}
	}
	return true
}

//判断字符串是不是字母+数字
func CtypeAlnum(s string) bool {

	if s == "" {
		return false
	}
	for _, r := range s {
		if !isDigit(r) && !isAlpha(r) {
			return false
		}
	}
	return true
}
