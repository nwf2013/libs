package rpcx

import (
	"log"
	"fmt"
	"time"
	"runtime/debug"
	"github.com/valyala/fasthttp"
	"github.com/buaazp/fasthttprouter"
)

var (
	route  *fasthttprouter.Router
	apiTimeoutMsg = `{"flags": "1","status": "error","status_code": 403,"message": "服务器响应超时，请稍后重试","data":null}`
	apiTimeout    = time.Second * 30
	fc            = &fasthttp.Client{}
	domain        = ""
)

func Def(path string, handle fasthttp.RequestHandler) {

	route.POST(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}



func NewServer() {
	
	route = fasthttprouter.New()

	route.PanicHandler = func(ctx *fasthttp.RequestCtx, p interface{}) {
		//进程异常处理
		err := p.(error)
		fmt.Println(err)
		debug.PrintStack()
		return
	}
}

func httpDoTimeout(requestBody []byte, method string, requestURI string, headers map[string]string, timeout time.Duration) ([]byte, int, error) {

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseResponse(resp)
		fasthttp.ReleaseRequest(req)
	}()

	req.SetRequestURI(requestURI)
	req.Header.SetMethod(method)

	switch method {
	case "POST":
		req.SetBody(requestBody)
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	// time.Second * 30
	err := fc.DoTimeout(req, resp, timeout)

	return resp.Body(), resp.StatusCode(), err
}

func NewClient(path string) {
	
	domain = path
}

func Call(path string, data []byte) (*Result){

	res := new(Result)

	requestURI   := domain+path
	body, _, err := httpDoTimeout(data, "POST", requestURI, nil, apiTimeout)
	if err != nil {
		res.Data  = err.Error()
		res.State = false
	} else {

		err := res.UnmarshalBinary(body)
		if err != nil {
			res.Data  = err.Error()
			res.State = false
		}
	}
	
	return res
}

func Start(addr string) {
	log.Fatal(fasthttp.ListenAndServe(addr, route.Handler))
}