package discovery

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"
	"go.etcd.io/etcd/clientv3"
)

var conn *clientv3.Client

func New(etcdAddr string) {

	var err error

	conn, err = clientv3.New(clientv3.Config{
		Endpoints:   strings.Split(etcdAddr, ";"),
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		log.Panic(err)
	}

}

func Get(key string) (*clientv3.GetResponse, error) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
    resp, err   := conn.Get(ctx, key)
    cancel()
	
	return resp, err

}

func Registry(name string, addr string, ttl int64) error {

	leaseResp, err := conn.Grant(context.Background(), ttl)
	if err != nil {
	   return err
	}
 
	_, err = conn.Put(context.Background(), "/"+name+"/"+addr, addr, clientv3.WithLease(leaseResp.ID))
	if err != nil {
	   fmt.Printf("put etcd error:%s",err)
	   return err
	}
 
	_, err = conn.KeepAlive(context.Background(), leaseResp.ID)
	if err != nil {
	   fmt.Printf("keep alive error:%s",err)
	   return err
	}

	return nil
}

func UnRegistry(name string, addr string) {

	conn.Delete(context.Background(), "/"+name+"/"+addr)
}

func Close() {

	conn.Close()
}


/*
func Register(etcdAddr, name string, addr string, ttl int64) error {
	var err error
 
	if cli == nil {
	   cli, err = clientv3.New(clientv3.Config{
		  Endpoints:   strings.Split(etcdAddr, ";"),
		  DialTimeout: 15 * time.Second,
	   })
	   if err != nil {
		  fmt.Printf("connect to etcd err:%s", err)
		  return err
	   }
	}
 
	ticker := time.NewTicker(time.Second * time.Duration(ttl))
 
	go func() {
	   for {
		  getResp, err := cli.Get(context.Background(), "/"+schema+"/"+name+"/"+addr)
		  //fmt.Printf("getResp:%+v\n",getResp)
		  if err != nil {
			 log.Println(err)
			 fmt.Printf("Register:%s", err)
		  } else if getResp.Count == 0 {
			 err = withAlive(name, addr, ttl)
			 if err != nil {
				log.Println(err)
				fmt.Printf("keep alive:%s", err)
			 }
		  } else {
			 //fmt.Printf("getResp:%+v, do nothing\n",getResp)
		  }
 
		  <-ticker.C
	   }
	}()
 
	return nil
 }
 
 func withAlive(name string, addr string, ttl int64) error {
	leaseResp, err := cli.Grant(context.Background(), ttl)
	if err != nil {
	   return err
	}
 
	//fmt.Printf("key:%v\n", "/"+schema+"/"+name+"/"+addr)
	_, err = cli.Put(context.Background(), "/"+schema+"/"+name+"/"+addr, addr, clientv3.WithLease(leaseResp.ID))
	if err != nil {
	   fmt.Printf("put etcd error:%s",err)
	   return err
	}
 
	_, err = cli.KeepAlive(context.Background(), leaseResp.ID)
	if err != nil {
	   fmt.Printf("keep alive error:%s",err)
	   return err
	}
	 return nil
 }
*/ 

