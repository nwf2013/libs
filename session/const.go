package session

import "time"

const defaultSessionKeyName = "dj"
const defaultDomain = ""
const defaultExpires = 2 * time.Hour
const defaultGCLifetime = 2 * time.Hour
//const defaultGCLifetime = 60 * time.Second
const defaultSecure = false
const defaultSessionIDInURLQuery = false
const defaultSessionIDInHTTPHeader = false
const defaultCookieLen uint32 = 32

const expirationAttributeKey = "_sid_"
