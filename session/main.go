package session

import (
	//"log"
	"fmt"
	"time"
	"errors"
	"github.com/valyala/fasthttp"
	"github.com/go-redis/redis/v7"
)

var (
	client *redis.Client
)

func New(cfg Config) {


	client = redis.NewClient(&redis.Options{
		Addr:         cfg.Addr,
		Password:     cfg.Auth,
		DB:           0,    // use default DB
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
		MaxRetries:   2,
		IdleTimeout:  5 * time.Minute,
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)
}

func Set(ctx *fasthttp.RequestCtx, value interface{}) bool {

	
	key := fmt.Sprintf("%d", Cputicks())
	err := client.SetNX(key, value, defaultGCLifetime).Err()
	if err != nil {
		return false
	}

	cookie := NewCookie()
	cookie.Set(ctx, defaultSessionKeyName, []byte(key), "", defaultExpires, defaultSecure)
	return true
}

func Destroy(ctx *fasthttp.RequestCtx) {

	cookie := NewCookie()
	key    := cookie.Get(ctx, defaultSessionKeyName)

	client.Unlink(string(key))
	cookie.Delete(ctx, defaultSessionKeyName)
}


func Get(ctx *fasthttp.RequestCtx) (interface{}, error) {

	key := string(ctx.Request.Header.Cookie(defaultSessionKeyName))

	val, err := client.Get(key).Result()
	if err == redis.Nil {
		return nil, errors.New("does not exist")
	} else if err != nil {
		return nil, err
	} else {
		return val, nil
	}
}

/*
func Replace(key string) interface{} {
	//return s.data.Get(key)
}
*/