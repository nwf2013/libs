package logger


import (
	"os"
	//"fmt"
	"time"
	"github.com/fluent/fluent-logger-golang/fluent"
)




var (
	f       *fluent.Fluent
	hostname string
)

func New(host string, port int) {

	var err error

	hostname, err = os.Hostname()
	if err != nil {
		panic(err)
	}

	f, err = fluent.New(fluent.Config{
		FluentPort: port, 
		FluentHost: host,
		Timeout   : 10 * time.Second,
		WriteTimeout : 10 * time.Second,
		MaxRetry : 3,
		Async  : false,
	})
  	if err != nil {
    	panic(err)
  	}

}

func Notice(message map[string]string) {
	
	tag := "NOTICE"
	message["hostname"] = hostname
	f.Post(tag, message)
}

func Warning(message map[string]string) {

	tag := "WARNING"

	message["hostname"] = hostname
	f.Post(tag, message)
}

func Error(message map[string]string) {

	tag := "ERROR"

	message["hostname"] = hostname
	f.Post(tag, message)
}

func Debug(message map[string]string) {

	tag := "DEBUG"

	message["hostname"] = hostname
	f.Post(tag, message)

}

func Info(message map[string]string) {

	tag := "INFO"

	message["hostname"] = hostname
	f.Post(tag, message)
}

func Emergency(message map[string]string) {

	tag := "EMERGENCY"

	message["hostname"] = hostname
	f.Post(tag, message)
}

func Alert(message map[string]string) {

	tag := "ALERT"

	message["hostname"] = hostname
	f.Post(tag, message)
}

func Critical(message map[string]string) {

	tag := "CRITICAL"
	message["hostname"] = hostname
	f.Post(tag, message)
}

func Close() {
	f.Close()
}