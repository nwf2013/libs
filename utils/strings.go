package utils

import (
    //"fmt"
    "math/rand"
    "strings"
    "time"

    "github.com/json-iterator/go"
)

var cjson = jsoniter.ConfigCompatibleWithStandardLibrary

func JsonEncode(v interface{}) ([]byte, error) {

    return cjson.Marshal(v)
}

func JsonDecode(str []byte) (map[string]interface{}, error) {

    reader  := strings.NewReader(string(str))
    decoder := cjson.NewDecoder(reader)
    params  := make(map[string]interface{})
    err     := decoder.Decode(&params)
    if err != nil {
        return params, err
    }

    return params, nil
}



/*
func GetBillNo(prefix, suffix string) string {
    return prefix + Format("YmdHis") + suffix
}
*/
//获取source的子串,如果start小于0或者end大于source长度则返回""
//start:开始index，从0开始，包括0
//end:结束index，以end结束，但不包括end
func Substring(source string, start int, end int) string {

    var r = []rune(source)
    length := len(r)

    if start < 0 || end > length || start > end {
        return ""
    }

    if start == 0 && end == length {
        return source
    }

    return string(r[start:end])
}

//字符串特殊字符转译
func Addslashes(str string) string {

    tmpRune := []rune{}
    strRune := []rune(str)
    for _, ch := range strRune {
        switch ch {
        case []rune{'\\'}[0], []rune{'"'}[0], []rune{'\''}[0]:
                tmpRune = append(tmpRune, []rune{'\\'}[0])
                tmpRune = append(tmpRune, ch)
        default:
                tmpRune = append(tmpRune, ch)
        }
    }
    return string(tmpRune)
}


func GetRandomString(length int) string {

    str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    bytes := []byte(str)
    var result []byte
    r := rand.New(rand.NewSource(time.Now().UnixNano()))
    for i := 0; i < length; i++ {
        result = append(result, bytes[r.Intn(len(bytes))])
    }
    return string(result)
}
